<?php
// Version: 2.0; Themes

global $scripturl;
$txt['maintenance'] = 'Site in Maintenance!';
$txt['approval_member'] = 'Member Approvals';
$txt['open_reports'] = 'Open Reports';
$txt['pm'] = 'pm';
$txt['forum_search'] = 'Search...';
$txt['wt_copyright'] = '<span><a href="https://www.solarus-games.org" target="_blank">Back to Solarus website</a></span>';

?>